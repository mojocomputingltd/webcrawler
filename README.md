# Overview

This is a gradle/groovy project which can be used to scrape the links for a given web page at a given base URL. Only links under the same base URL 
will be scraped. Details of external links and images, javascript and CSS static resources will also be shown.

# Development

## Design assumptions
* Gradle and groovy has been used to make it easy to run from the command line and to manage groovy/java dependencies
* The 3rd party library jsoup (https://jsoup.org/) has been used to avoid re-inventing the
wheel for parsing HTML to extract elements we require. jsoup also handles most errors
with invalid HTML.

## Pre-requisites

### Java JDK
* JDK 8 (1.8.0_91) : **It needs to be the JDK, not JRE**

The resulting test results can be viewed by opening build/reports/test/index.html

# Runing the application from the command line

The Gradle task (crawlBaseUrl : see build.gradle) can be run using the supplied gradle wrapper.

To run the application issue the following, from the root directory of the project.

```
./gradlew clean crawlBaseUrl -Purl=<base url>
```

where:-
```
<base url> is the base URL to start scraping from (e.g. https://buildit.wiprodigital.com/)
```

# Running the unit tests

```
./gradlew clean test
```

# TODO
* Write integration tests to validate the actual usage of jsoup in HtmlScrapeServiceImpl, which
did not easily lend themselves to unit test mocking.
* Optimise performance by saving scraped pages to a datastore with last modified details to avoid
re-scraping pages that had not changed (for re-runs or cron jobs)
* Potentially make it multi-threaded to optimise performance
* Respect use of robots.txt on visited websites
* Add option to limit depth of crawl
