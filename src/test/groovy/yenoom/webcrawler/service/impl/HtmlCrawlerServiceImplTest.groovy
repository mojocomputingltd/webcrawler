package yenoom.webcrawler.service.impl

import spock.lang.Specification
import yenoom.webcrawler.domain.LinkedHtmlPage
import yenoom.webcrawler.domain.ScrapedPage
import yenoom.webcrawler.service.HtmlScrapeService

class HtmlCrawlerServiceImplTest extends Specification {

    HtmlCrawlerServiceImpl htmlCrawlerServiceImpl

    private static final String TEST_BASE_URL = 'https://foo.bar.com'

    HtmlScrapeService mockHtmlScrapeService
    def mockPageScraped

    def setup() {
        mockHtmlScrapeService = Mock(HtmlScrapeService)
        htmlCrawlerServiceImpl = new HtmlCrawlerServiceImpl(mockHtmlScrapeService)
    }

    def "Crawling a URL with no child linked pages returns details for just the base URL"() {
        given: 'A website has no child linked pages'
        mockPageScraped = new ScrapedPage(TEST_BASE_URL)

        and: 'The mocked service returns the mocked pages scraped'
        1 * mockHtmlScrapeService.scrapePage(TEST_BASE_URL) >> mockPageScraped

        when: 'A call to scrape that website is made'
        def actualScrapedPages = htmlCrawlerServiceImpl.crawlFromBaseUrl(TEST_BASE_URL)

        then: 'The scraped pages returned show only the main URL was scraped'
        actualScrapedPages.size() == 1

        and: 'The page returned is for the base URL'
        actualScrapedPages[0].pageUrl == TEST_BASE_URL

    }

    def "Crawling a URL with 1 child linked internal page returns details for the child linked internal page"() {
        given: 'A website has 1 child linked internal page'
        mockPageScraped = new ScrapedPage(TEST_BASE_URL)
        def internalPageLink = TEST_BASE_URL + '/page1'
        mockPageScraped.linkedPages << new LinkedHtmlPage('href', 'link text', internalPageLink)

        and: 'The mocked service returns the mocked pages scraped'
        1 * mockHtmlScrapeService.scrapePage(TEST_BASE_URL) >> mockPageScraped
        1 * mockHtmlScrapeService.scrapePage(internalPageLink) >> new ScrapedPage(internalPageLink)

        when: 'A call to scrape that website is made'
        def actualScrapedPages = htmlCrawlerServiceImpl.crawlFromBaseUrl(TEST_BASE_URL)

        then: 'The scraped pages returned shows the main URL and an internal child page was scraped'
        actualScrapedPages.size() == 2

        and: 'The pages returned are for the base URL and the internal child page'
        actualScrapedPages[0].pageUrl == TEST_BASE_URL
        actualScrapedPages[1].pageUrl == internalPageLink

    }

    def "Crawling a URL with 2 child linked internal pages for the same URL returns details for only 1 child linked internal page"() {
        given: 'A website has 2 child linked internal pages for the same URL'
        mockPageScraped = new ScrapedPage(TEST_BASE_URL)
        def internalPageLink = TEST_BASE_URL + '/page1'
        mockPageScraped.linkedPages << new LinkedHtmlPage('href', 'link 1 text', internalPageLink)
        mockPageScraped.linkedPages << new LinkedHtmlPage('href', 'link 2 text', internalPageLink)

        and: 'The mocked service returns the mocked pages scraped and only scrapes the child page once'
        1 * mockHtmlScrapeService.scrapePage(TEST_BASE_URL) >> mockPageScraped
        1 * mockHtmlScrapeService.scrapePage(internalPageLink) >> new ScrapedPage(internalPageLink)

        when: 'A call to scrape that website is made'
        def actualScrapedPages = htmlCrawlerServiceImpl.crawlFromBaseUrl(TEST_BASE_URL)

        then: 'The scraped pages returned shows the main URL and an internal child page was scraped'
        actualScrapedPages.size() == 2

        and: 'The pages returned are for the base URL and the internal child page'
        actualScrapedPages[0].pageUrl == TEST_BASE_URL
        actualScrapedPages[1].pageUrl == internalPageLink

    }

    def "Crawling a URL with 1 child linked external page returns details for just the base URL"() {
        given: 'A website has 1 child linked external page'
        mockPageScraped = new ScrapedPage(TEST_BASE_URL)
        def internalPageLink = 'https://not.foo.bar.com'
        mockPageScraped.linkedPages << new LinkedHtmlPage('href', 'link text', internalPageLink)

        and: 'The mocked service returns the mocked pages scraped'
        1 * mockHtmlScrapeService.scrapePage(TEST_BASE_URL) >> mockPageScraped

        when: 'A call to scrape that website is made'
        def actualScrapedPages = htmlCrawlerServiceImpl.crawlFromBaseUrl(TEST_BASE_URL)

        then: 'The scraped pages returned shows the main URL and an internal child page was scraped'
        actualScrapedPages.size() == 1

        and: 'The page returned is for the base URL'
        actualScrapedPages[0].pageUrl == TEST_BASE_URL

    }

    def "Crawling a URL with a child linked page for a javascript anchor link returns details for just the base URL"() {
        given: 'A website has no child linked pages'
        mockPageScraped = new ScrapedPage(TEST_BASE_URL)
        def internalPageLink = 'https://foo.bar.com#javascript-stuff'
        mockPageScraped.linkedPages << new LinkedHtmlPage('#javascript-stuff', 'link text', internalPageLink)

        and: 'The mocked service returns the mocked pages scraped'
        1 * mockHtmlScrapeService.scrapePage(TEST_BASE_URL) >> mockPageScraped

        when: 'A call to scrape that website is made'
        def actualScrapedPages = htmlCrawlerServiceImpl.crawlFromBaseUrl(TEST_BASE_URL)

        then: 'The scraped pages returned show only the main URL was scraped'
        actualScrapedPages.size() == 1

        and: 'The page returned is for the base URL'
        actualScrapedPages[0].pageUrl == TEST_BASE_URL

    }

}
