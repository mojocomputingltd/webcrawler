package yenoom.webcrawler.application

import yenoom.webcrawler.domain.ScrapedPage;
import yenoom.webcrawler.service.HtmlCrawlerService
import yenoom.webcrawler.service.impl.HtmlCrawlerServiceImpl
import yenoom.webcrawler.service.impl.HtmlScrapeServiceImpl;

public class WebCrawler {

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new RuntimeException("No Base URL supplied")
        }
        // TODO : the implementation classes could be injected if Spring boot/ webapp was being used
        HtmlCrawlerService pageScraper = new HtmlCrawlerServiceImpl(new HtmlScrapeServiceImpl() )
        List<ScrapedPage> scrapedPages = pageScraper.crawlFromBaseUrl(args[0])
        println "\n\n\n**** ${scrapedPages.size()} Pages Scraped ****\n"
        scrapedPages.each {scrapedPage ->
            // TODO : for now just simple console output for each internal page found using toString() of ScrapedPage
            println(scrapedPage)
        }
    }
}
