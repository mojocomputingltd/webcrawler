package yenoom.webcrawler.domain

/**
 * Represents a single scraped page with details of required HTML elements
 */
class ScrapedPage {

    final String pageUrl
    List<LinkedHtmlPage> linkedPages = []

    // TODO : Following are not resolved to absolute (if needed ?)
    List<String> imageSources = []
    List<String> javascriptSources = []
    List<String> links = [] // TODO : could be course-grained object holding details of type of 'rel' for each link

    /**
     * Mandatory constructor for representing a scraped page.
     * @param pageUrl The absolute URL of the page being scraped.
     */
    ScrapedPage(String pageUrl) {
        this.pageUrl = pageUrl
    }

    @Override
    public String toString() {
        return "**************************************************************************\n" +
                "Domain Page Url='" + pageUrl + '\'' +
                "\n\n"  +
                "linkedPages=\n  " + linkedPages.join('\n  ') +
                "\n\n"  +
                "imageSources=\n  " + imageSources.join('\n  ') +
                "\n\n"  +
                "javascriptSources=\n  " + javascriptSources.join('\n  ') +
                "\n\n"  +
                "links=\n  " + links.join('\n  ') +
                "\n\n\n"
    }
}
