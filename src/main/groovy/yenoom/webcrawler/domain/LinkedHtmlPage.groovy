package yenoom.webcrawler.domain

/**
 * Represents a link to a page from another page. The page could be for either an internal
 * or external page.
 */
class LinkedHtmlPage {

    final String linkHref
    final String absHref
    final String linkText

    /**
     * Mandatory constructor for representing a linked page.
     * @param linkHref The link href to the page, as described in the referencing HTML (could be relative or absolute)
     * @param linkText The text of the link to the page, as described in the referencing
     * @param absHref The absolute URL href of the page
     */
    LinkedHtmlPage(String linkHref, String linkText, String absHref) {
        this.linkHref = linkHref
        this.linkText = linkText
        this.absHref = absHref
    }


    @Override
    public String toString() {
        return  "linkHref='" + linkHref + '\'' +
                ", linkText='" + linkText + '\'' +
                ", absHref='" + absHref + '\''
    }
}
