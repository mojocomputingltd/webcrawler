package yenoom.webcrawler.service

import yenoom.webcrawler.domain.ScrapedPage

interface HtmlScrapeService {

    /**
     * Scrapes the content of a page at a given URL
     *
     * @param pageUrl The URL of the page to scrape
     * @return A representation of the page scraped with relevant elements extracted
     */
    ScrapedPage scrapePage(String pageUrl)

}
