package yenoom.webcrawler.service

import yenoom.webcrawler.domain.ScrapedPage

interface HtmlCrawlerService {

    /**
     * Scrapes the content of a website starting from a given base domain URL.
     * Only pages under the same base domain URL are scraped
     *
     * @param baseUrl The base URL of the website to scrape
     * @return A list of all the linked pages under the same base domain URL scraped with relevant elements extracted
     */
    List<ScrapedPage> crawlFromBaseUrl(String baseUrl)

}
