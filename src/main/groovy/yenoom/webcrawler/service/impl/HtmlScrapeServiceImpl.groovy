package yenoom.webcrawler.service.impl

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import yenoom.webcrawler.domain.LinkedHtmlPage
import yenoom.webcrawler.domain.ScrapedPage
import yenoom.webcrawler.service.HtmlScrapeService

class HtmlScrapeServiceImpl implements HtmlScrapeService {

    // TODO : write integration test for real usage of 3rd party library
    @Override
    ScrapedPage scrapePage(String pageUrl) {
        ScrapedPage scrapedPage = new ScrapedPage(pageUrl)
        // Get the document just once and then re-use via helper methods to extract elements
        Document doc = Jsoup.connect(pageUrl).get()
        scrapedPage.linkedPages = getLinkedPages(doc)
        scrapedPage.imageSources = getSimpleElementsAsString(doc, 'img', 'src')
        scrapedPage.javascriptSources = getSimpleElementsAsString(doc, 'script', 'src')
        scrapedPage.links = getSimpleElementsAsString(doc, 'link', 'href')
        scrapedPage
    }

    private List<LinkedHtmlPage> getLinkedPages(Document doc) {
        def linkedPages = []
        Elements links = doc.getElementsByTag("a")
        for (Element link : links) {
            String linkRef = link.attr("href")
            String absHref = link.attr("abs:href");
            String linkText = link.text()
            linkedPages << new LinkedHtmlPage(linkRef, linkText, absHref)
        }
        linkedPages
    }

    private List<String> getSimpleElementsAsString(Document doc, String elementTagName, String elementAttributeKey) {
        def simpleElements = []
        Elements links = doc.getElementsByTag(elementTagName)
        for (Element link : links) {
            simpleElements << link.attr(elementAttributeKey)
        }
        simpleElements
    }

}
