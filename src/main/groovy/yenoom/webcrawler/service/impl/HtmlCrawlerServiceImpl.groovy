package yenoom.webcrawler.service.impl

import yenoom.webcrawler.domain.LinkedHtmlPage
import yenoom.webcrawler.domain.ScrapedPage
import yenoom.webcrawler.service.HtmlCrawlerService
import yenoom.webcrawler.service.HtmlScrapeService

class HtmlCrawlerServiceImpl implements HtmlCrawlerService {

    HtmlScrapeService htmlScrapeService

    public HtmlCrawlerServiceImpl(HtmlScrapeService htmlScrapeService) {
        this.htmlScrapeService = htmlScrapeService // Injected via constructor to allow for mocking in tests
    }

    @Override
    public List<ScrapedPage> crawlFromBaseUrl(String baseUrl) {
        List<ScrapedPage> pagesScraped = [] // Cumulative list of pages scraped
        crawl(baseUrl, baseUrl, pagesScraped)
        pagesScraped
    }

    private void crawl(String baseUrl, String pageUrl, List<ScrapedPage> pagesScraped) {
        println "Scraping $pageUrl" // TODO : log4j output
        def scrapedPage = htmlScrapeService.scrapePage(pageUrl)
        pagesScraped << scrapedPage
        scrapedPage.linkedPages.each {LinkedHtmlPage linkedPage ->
            if (!linkedPage.linkHref.startsWith("#")) { // Exclude hash links (e.g. for javascript)
                if (linkedPage.absHref.startsWith(baseUrl)) { // assume case-insensitive
                    if (!pagesScraped.find {it.pageUrl == linkedPage.absHref}) { // Do not re-scrape pages
                        crawl(baseUrl, linkedPage.absHref, pagesScraped)
                    }
                }
            }
        }
    }

}
